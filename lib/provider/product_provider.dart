import 'package:flutter/cupertino.dart';
import '../provider/products.dart';

class ProductProvider with ChangeNotifier{
  List<Product> _prodcuts = 
  [
    Product(id: '1', title: 'Pen', description: 'wall pen', price: 9.99, imgUrl: 'none'),
    Product(id: '2', title: 'Shoes', description: 'sports sheo', price: 999.99, imgUrl: 'none'),
    Product(id: '3', title: 'T-shirt', description: 'black t-shirt', price: 150.00, imgUrl: 'none'),
    Product(id: '4', title: 'Pen 1', description: 'wall pen', price: 9.99, imgUrl: 'none'),
    Product(id: '5', title: 'Shoes 1', description: 'sports sheo', price: 999.99, imgUrl: 'none'),
    Product(id: '6', title: 'T-shirt 1', description: 'black t-shirt', price: 150.00, imgUrl: 'none'),
  ];

  //getter
  List<Product> get items {
    //spread for non-reference
    return [..._prodcuts];
  }

  List<Product> get getFavorite{
    return _prodcuts.where((prod) => prod.isFavorite).toList();
    // return [..._prodcuts];
    
  }

  Product findById(String id){
    return _prodcuts.firstWhere((prod) => id == prod.id);
  }

  void addProduct(){
    notifyListeners();
  }
}