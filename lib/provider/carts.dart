import 'package:flutter/foundation.dart';

class CartItem{
  final String id;
  final String title;
  final int count;
  final double price;

  CartItem(
    {
      @required this.id, 
      @required this.title, 
      @required this.count, 
      @required this.price,
    }
  );

}

class Cart with ChangeNotifier{

  Map<String, CartItem> _items = {};

  Map<String, CartItem> get cart{
    return {..._items};
  }

  int get itemCount{
    return _items.length;
  }

  void addItem(String productId, double price, String title){

    if(_items.containsKey(productId)){
      _items.update(
        productId, (prod) => CartItem(
          id: prod.id, 
          price: prod.price, 
          title: prod.title, 
          count: prod.count+1
        )
      );
    }
    else{
      _items.putIfAbsent(productId, () => CartItem(
        id: DateTime.now().toString(), 
        price: price, 
        count:  1,
        title: title
        )
      );
    }
    notifyListeners();
  }
}

