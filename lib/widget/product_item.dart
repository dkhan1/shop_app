import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/products.dart';
import '../screen/product_detail.dart';
import '../provider/carts.dart';

class ProductItem extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    var product = Provider.of<Product>(context, listen: false);
    var cart = Provider.of<Cart>(context, listen: false);

    return GridTile(    
        child: GestureDetector(
          child: Image.network(
            'https://dummyimage.com/200x200/444/fff.png&text=${product.title}',
            fit: BoxFit.cover
          ),
          onTap: (){
            Navigator.pushNamed(
              context, ProductDetail.path, 
              arguments: product.id
            );
          },
        ),
        footer: GridTileBar(
          leading: Consumer<Product>(
            builder: (contxt, product, child) => IconButton( 
              icon: Icon(product.isFavorite? Icons.favorite : Icons.favorite_border),
              onPressed: (){
                product.toggleFavorite();
              },
            ),
          ),
          trailing: IconButton( 
            icon: Icon(Icons.shopping_cart),
            onPressed: (){
              cart.addItem(product.id, product.price, product.title);
            },
          ),
          title: Text(product.title, textAlign: TextAlign.center),
          backgroundColor: Colors.black54
        ),
    );
  }
}