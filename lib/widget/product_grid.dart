import 'package:flutter/cupertino.dart';
import './product_item.dart';
import 'package:provider/provider.dart';
import '../provider/product_provider.dart';

class ProductGrid extends StatelessWidget{
  final bool showFav;
  ProductGrid(this.showFav);

  @override
  Widget build(BuildContext context) {
    final providerInstanse = Provider.of<ProductProvider>(context);
    final productlist = showFav ? providerInstanse.getFavorite : providerInstanse.items;
    return GridView.builder(
        itemCount: productlist.length,
        itemBuilder: (ctx, index) => ChangeNotifierProvider.value(
          value: productlist[index],
          child: ProductItem(),
        ) ,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, //column count
          crossAxisSpacing: 10, //10px space between item
          mainAxisSpacing: 10,
          childAspectRatio: 2/3,
        ), 
      );
  }
}