import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/screen/product_detail.dart';
import './screen/product_overview.dart';
import './provider/product_provider.dart';
import './provider/carts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ProductProvider()),
        ChangeNotifierProvider(create: (_) => Cart()),
      ], //or use builder instead create
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        home: ProductOverviewScreen(),
        routes: {
          ProductOverviewScreen.path: (context) => ProductOverviewScreen(),
          ProductDetail.path: (context) => ProductDetail(),
        },
      ),
    );
  }
}
