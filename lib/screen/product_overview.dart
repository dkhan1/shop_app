import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/provider/carts.dart';
import 'package:shop_app/widget/badge.dart';
import 'package:shop_app/widget/product_grid.dart';

enum menuOption{
  Favorite,
  ShowAll,
}

class ProductOverviewScreen extends StatefulWidget{
  static final String path = '/products';
  ProductOverviewScreenState createState() =>  ProductOverviewScreenState();
}

class ProductOverviewScreenState extends State<ProductOverviewScreen>{

  var _showFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shop App'),
        actions: <Widget>[
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            onSelected: (menuOption selected){
              setState(() {
                if(selected == menuOption.ShowAll){

                  _showFavorite = false;
                }else if(selected == menuOption.Favorite){
                  _showFavorite = true;
                }
                print(selected);
              });
              
            },
            itemBuilder: (ctx) => [
              PopupMenuItem(child: Text('Show all'), value: menuOption.ShowAll),
              PopupMenuItem(child: Text('Favorite'), value: menuOption.Favorite,)
          ]),
          Consumer<Cart>(
            builder: (_, cartData, _2) => Badge(
              value: cartData.itemCount.toString(),
              child: IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: (){},
              ),
            )
          )

        ],
      ),
      body: ProductGrid(_showFavorite),
    );
  }

}