import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/product_provider.dart';

class ProductDetail extends StatelessWidget{
  static final String path = '/product_detail';

  @override 
  Widget build(BuildContext context) {

    var id = ModalRoute.of(context).settings.arguments as String;
    
    var item = Provider.of<ProductProvider>(context, listen: false).findById(id);

    return Scaffold(
      appBar: AppBar(
        title: Text(item.title),
      ),

    );
  }
}